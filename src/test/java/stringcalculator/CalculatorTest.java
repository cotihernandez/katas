package stringcalculator;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;


public class CalculatorTest {

    //AAA Arrange, Act y Assert

    @Test
    public void shouldReturnZeroWhenStringIsEmpty(){
        //Arrange
        Calculator calculator = new Calculator();
        String number = "";

        //Act
        int result = calculator.add(number);

        //Assert
        Assert.assertThat(0,is(result));
    }

    @Test
    public void shouldReturnSameNumberWhenIsOnlyOneNumber(){
        //Arrange
        Calculator calculator = new Calculator();
        String number = "2";

        //Act
        int result = calculator.add(number);

        //Assert
        Assert.assertThat(2,is(result));
    }

    @Test
    public void shouldReturnTheSumForTwoNumbers(){
        //Arrange
        Calculator calculator = new Calculator();
        String number = "2,3";

        //Act
        int result = calculator.add(number);

        //Assert
        Assert.assertThat(5,is(result));

    }

}