package stringcalculator;

public class Calculator {

    public int add(String number) {


        if(number.isEmpty())
            return 0;

        String[] numbers = number.split(",");
        if (numbers.length > 1)
            return Integer.parseInt(numbers[0]) + Integer.parseInt(numbers[1]);
        return Integer.parseInt(number);
    }
}
